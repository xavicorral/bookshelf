import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import SearchToolbar from '../searchToolbar';

const props = {
    handleToolbarSubmit: jest.fn()
}

test('SearchToolbar is rendered', () => {
    const { getByText, getByRole } = render(<SearchToolbar {...props} />);
    const discoverParagraph = getByText('Welcome to the discover page.')
    expect(discoverParagraph).toBeInTheDocument();
    const button = getByRole('button');
    fireEvent.click(button)
    expect(props.handleToolbarSubmit).toHaveBeenCalledTimes(1)
})