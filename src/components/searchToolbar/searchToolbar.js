import React from 'react';
import './searchToolbar.scss';
import { FaSearch } from 'react-icons/fa';

const SearchToolbar = (props) => {

    const handleSubmit = (event) => {
        const searchTerm = event.target.elements[0].value;
        props.handleToolbarSubmit(searchTerm)
        event.preventDefault();
    }

    return (
        <>
            <div className='toolbar' onSubmit={(event) => handleSubmit(event)}>
                <form action="">
                    <input type='text' placeholder='Search books (ex: React)...' />
                    <button type='submit' className='fa-search'><FaSearch /></button>
                </form>
            </div>
            <div className='discoverText'>
                <p>Welcome to the discover page.</p>
                <p>Here, let me load a few books for you...</p>
            </div>
        </>
    );
};

export default SearchToolbar;