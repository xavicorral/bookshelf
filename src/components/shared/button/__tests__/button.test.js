import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Button from '../button';

let props = {
    active: true,
    onClickHandler: jest.fn(),
    name: 'login'
}

test('renders Button component', () => {
    const { getByText } = render(<Button {...props} />);
    const linkElement = getByText(/login/i);
    expect(linkElement).toBeInTheDocument();
});

test('check that the button has the active class when the flag is raised', () => {
    const { getByText, getByRole } = render(<Button {...props} />);
    expect(getByRole('button')).toHaveClass('active')
})

test('checks that handler is called when clicking the button', () => {
    const { getByText, getByRole } = render(<Button {...props} />);
    fireEvent.click(getByText(/login/))
    expect(props.onClickHandler).toHaveBeenCalledTimes(1)
})