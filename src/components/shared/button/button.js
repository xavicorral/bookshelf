import React from 'react';
import './button.scss';

const Button = ({ active, onClickHandler, name }) => {
    const className = `customButton ${active ? 'active' : ''}`
    return (
        <button className={className} onClick={onClickHandler}>
            {name}
        </button>
    );
};

export default Button;