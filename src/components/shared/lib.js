/** @jsx jsx */
import { jsx } from '@emotion/core'

import { keyframes } from '@emotion/core'
import { FaSpinner } from 'react-icons/fa'
import styled from '@emotion/styled'
import * as colors from '../../styles/colors'

const spin = keyframes({
    '0%': { transform: 'rotate(0deg)' },
    '100%': { transform: 'rotate(360deg)' },
})

export function Spinner(props) {
    return (
        <FaSpinner
            css={{ animation: `${spin} 1s linear infinite` }}
            aria-label="loading"
            {...props}
        />
    )
}

export function FullPageSpinner() {
    return (
        <div css={{ marginTop: '3em', fontSize: '4em', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Spinner />
        </div>
    )
}

export function removeDuplicates(array) {
    return array.filter((a, b) => array.indexOf(a) === b)
};

export const CircleButton = styled.button({
    fontSize: '15px',
    fontWeight: 'bolder',
    borderRadius: '30px',
    padding: '0',
    width: '40px',
    height: '40px',
    lineHeight: '1',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    background: colors.base,
    color: colors.text,
    border: `1px solid ${colors.gray10}`,
    cursor: 'pointer',
})

export const FormGroup = styled.div({
    display: 'flex',
    flexDirection: 'column',
})
