import React from 'react';
import Book from './book';
import { useBooksList } from '../../context/use-books-list';
import { readBookAction, finishBookAction, deleteBookAction } from '../../actions/actions';

const BooksList = (props) => {
    const { dispatch } = useBooksList()
    let handlers = {}

    const handleAddItemClick = (event) => {
        const id = event.currentTarget.id

        switch (props.listType) {
            case 'discover':
                dispatch(readBookAction(id))
                break;
            case 'list':
                dispatch(finishBookAction(id))
                break;
            default:
                break;
        }
    }

    const handleDeleteItemClick = (event) => {
        const id = event.currentTarget.id
        dispatch(deleteBookAction(id))
    }

    switch (props.listType) {
        case 'discover':
            handlers = { ...handlers, handleAddItemClick: handleAddItemClick }
            break;
        case 'list':
            handlers = { ...handlers, handleAddItemClick: handleAddItemClick, handleDeleteItemClick: handleDeleteItemClick }
            break;
        case 'finished':
            handlers = { ...handlers, handleDeleteItemClick: handleDeleteItemClick }
            break;
        default:
            break;
    }

    return (
        <div>
            {props.books
                ? props.books.map((book, i) => <Book key={i} book={book} handlers={handlers} />)
                : null
            }
        </div>
    );
};

export default BooksList;