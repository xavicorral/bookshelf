import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Book from '../book';

const props = {
    book: {
        isbn13: 'isbn13',
        title: 'react title',
        subtitle: 'react subtitle',
        image: 'xxx',
        url: 'xxx'
    },
    handlers: {
        handleAddItemClick: jest.fn(),
        handleDeleteItemClick: jest.fn()
    }
}

test('renders Book component', () => {
    const { getByText } = render(<Book {...props} />);
    const titleElement = getByText(/react title/i);
    expect(titleElement).toBeInTheDocument();
    const subtitleElement = getByText(/react subtitle/i);
    expect(subtitleElement).toBeInTheDocument();
});

test('check event is fired when clicking add button', () => {
    const { getByTestId } = render(<Book {...props} />);
    fireEvent.click(getByTestId(/isbn13add/i))
    expect(props.handlers.handleAddItemClick).toHaveBeenCalledTimes(1)
})

test('check event is fired when clicking delete button', () => {
    const { getByTestId } = render(<Book {...props} />);
    fireEvent.click(getByTestId(/isbn13delete/i))
    expect(props.handlers.handleDeleteItemClick).toHaveBeenCalledTimes(1)
})