import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import BooksList from '../booksList';
import * as useBooksContext from '../../../context/use-books-list';
import { finishBookAction, deleteBookAction } from '../../../actions/actions';

useBooksContext.useBooksList = jest.fn().mockReturnValue({
    dispatch: jest.fn()
});

const props = {
    books: [{
        isbn13: 'isbn13',
        title: 'react title',
        subtitle: 'react subtitle',
        image: 'xxx',
        url: 'xxx'
    }],
    listType: 'list',
    handlers: {
        handleAddItemClick: jest.fn(),
        handleDeleteItemClick: jest.fn()
    }
}

test('renders BooksList component', () => {
    const { getByText } = render(<BooksList {...props} />);
    const titleElement = getByText(/react title/i);
    expect(titleElement).toBeInTheDocument();
    const subtitleElement = getByText(/react subtitle/i);
    expect(subtitleElement).toBeInTheDocument();
});

test('check an event is dispatched when clicking add button in the discover list', () => {
    const { getByTestId } = render(<BooksList {...props} />);
    fireEvent.click(getByTestId(/isbn13add/i))
    expect(useBooksContext.useBooksList().dispatch).toHaveBeenCalledTimes(1)
    expect(useBooksContext.useBooksList().dispatch).toHaveBeenCalledWith(finishBookAction('isbn13'))
})

test('check an event is dispatched when clicking delete button in the discover list', () => {
    const { getByTestId } = render(<BooksList {...props} />);
    fireEvent.click(getByTestId(/isbn13delete/i))
    expect(useBooksContext.useBooksList().dispatch).toHaveBeenCalledTimes(2)
    expect(useBooksContext.useBooksList().dispatch).toHaveBeenCalledWith(deleteBookAction('isbn13'))
})
