import React from 'react';
import './booksList.scss';
import { FaPlusCircle, FaMinusCircle } from 'react-icons/fa';

const Book = (props) => {
    const { isbn13, title, subtitle, image, url } = props.book

    return (
        <div className='bookWrapper'>
            <div className='book'>
                <div className='bookImage'>
                    <img src={image} alt='book sleeve'></img>
                </div>
                <div className='bookText'>
                    <div className='bookHeader'>
                        <div className='bookTitle'>
                            <a href={url} target='_blank' rel="noopener noreferrer">{title}</a>
                        </div>
                    </div>
                    <div className='bookDescription'>
                        {subtitle}
                    </div>
                </div>
            </div>
            <div className='bookActionIcon' >
                {props.handlers.handleAddItemClick ?
                    <div className='actionIcon' id={isbn13} data-testid={isbn13 + 'add'} onClick={props.handlers.handleAddItemClick}><FaPlusCircle /></div>
                    :
                    null}
                {props.handlers.handleDeleteItemClick ?
                    <div className='actionIcon' id={isbn13} data-testid={isbn13 + 'delete'} onClick={props.handlers.handleDeleteItemClick}><FaMinusCircle /></div>
                    :
                    null}
            </div>
        </div>
    );
};

export default Book;