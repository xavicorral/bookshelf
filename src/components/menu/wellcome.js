import React from 'react';
import Button from '../shared/button/button';
import { useAuth } from '../../context/use-auth';

const Wellcome = () => {
    const auth = useAuth();

    return (
        <header>
            {auth.user && <p>Wellcome, {auth.user.email.slice(0, 10)}</p>}
            <Button name='Logout' active='true' onClickHandler={auth.signOut} />
        </header>
    );
};

export default Wellcome;