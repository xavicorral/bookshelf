import React from 'react';
import { Link, useHistory } from "react-router-dom";
import './menu.scss';

const Menu = () => {

    const { location } = useHistory();

    return (
        <aside>
            <ul>
                <li className={location.pathname === '/list' ? 'active' : null}>
                    <Link to="/list" className='menuItem'>
                        Reading List
                    </Link>
                </li>
                <li className={location.pathname === '/finished' ? 'active' : null}>
                    <Link to="/finished" className='menuItem'>
                        Finished Books
                    </Link>
                </li>
                <li className={location.pathname === '/discover' ? 'active' : null}>
                    <Link to="/discover" className='menuItem'>
                        Discover
                    </Link>
                </li>
            </ul>
        </aside>
    );
};

export default Menu;