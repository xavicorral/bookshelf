import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Wellcome from '../Wellcome';
import * as useContext from '../../../context/use-auth';

useContext.useAuth = jest.fn().mockReturnValue(
    {
        user: { email: 'pepito@gmail.com' },
        signOut: jest.fn()
    });

test('renders Wellcome component with the user email trunked', () => {
    const { getByText } = render(<Wellcome />);
    const emailElement = getByText(/Wellcome, pepito@gma/i);
    expect(emailElement).toBeInTheDocument();
});

test('check that signout is called when clicking the button', () => {
    const { getByText } = render(<Wellcome />);
    const logoutButton = getByText(/Logout/i);
    expect(logoutButton).toBeInTheDocument();
    fireEvent.click(logoutButton)
    expect(useContext.useAuth().signOut).toHaveBeenCalledTimes(1)
});