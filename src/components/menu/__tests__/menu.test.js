import React from 'react';
import { render } from '@testing-library/react';
import Menu from '../menu';

jest.mock('react-router-dom', () => ({
    useHistory: () => ({
        push: jest.fn(),
        location: { pathname: '/list' }
    }),
    Link: (props) => (<a href={props.to}>{props.children}</a>)
}));

test('renders Menu component', () => {
    const { getByText } = render(<Menu />);
    const menuElement = getByText(/Reading List/i);
    expect(menuElement).toBeInTheDocument();
});
