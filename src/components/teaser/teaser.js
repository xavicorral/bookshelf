import React from 'react';
import { Link } from "react-router-dom";
import './teaser.scss';

const Teaser = (props) => {
    return (
        <div>
            {props.path === 'list' ?
                <p>Looks like you've finished all your books! Check them out in your <Link to='finished'>finished books</Link> or <Link to='discover'>discover</Link> more.</p>
                :
                <p>Hey there! This is where books will go when you've finished reading them. Get started by heading over to the <Link to='discover'>discover</Link> page to add books to your list.</p>
            }
        </div>
    );
};

export default Teaser;