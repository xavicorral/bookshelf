import React from 'react';
import { render } from '@testing-library/react';
import Teaser from '../teaser';

jest.mock('react-router-dom', () => ({
    Link: (props) => (<a href={props.to}>{props.children}</a>)
}));


test('teaser component renders list text teaser', () => {
    const props = { path: 'list' }
    const { getByText } = render(<Teaser {...props} />);
    const linkElement = getByText('finished books');
    expect(linkElement).toBeInTheDocument();
});

test('teaser component does not render list text teaser', () => {
    const props = { path: 'discover' }
    const { getByText } = render(<Teaser {...props} />);
    const linkElement = getByText('discover');
    expect(linkElement).toBeInTheDocument();
})
