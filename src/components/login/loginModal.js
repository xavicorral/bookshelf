
import React from 'react';
import Button from '../shared/button/button';
import { useAuth } from '../../context/use-auth';
import { Dialog } from '@reach/dialog';
import VisuallyHidden from '@reach/visually-hidden';
import "@reach/dialog/styles.css";
import { CircleButton, FormGroup } from '../shared/lib'
import './modal.scss'

const LoginModal = () => {
    const auth = useAuth();
    const [isOpen, setIsOpen] = React.useState(false)

    const handleLogin = (e) => {
        e.preventDefault();
        console.log('logging')
        const email = document.querySelector('#email').value;
        const password = document.querySelector('#password').value;
        auth.signIn(email, password);
    }

    const open = () => { console.log('opening'); setIsOpen(true) };
    const close = () => setIsOpen(false);

    return (
        <>
            <Button name='Login' onClickHandler={open} active='true' className='button active'>Login</Button>
            <Dialog isOpen={isOpen} onDismiss={close} aria-label='login'>
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <CircleButton onClick={() => setIsOpen(false)}>
                        <VisuallyHidden>Close</VisuallyHidden>
                        <span aria-hidden>×</span>
                    </CircleButton>
                </div>
                <div>
                    <h1>Login</h1>
                    <form className='formClass' onSubmit={handleLogin}>
                        <FormGroup>
                            <label htmlFor="password">Email</label>
                            <input type='text' name='email' id='email'></input>
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="password">Password</label>
                            <input type='password' name='password' id='password'></input>
                        </FormGroup>
                        <div>
                            <Button name='Login' active='true' onClickHandler={handleLogin} />
                        </div>
                    </form>
                </div>
            </Dialog>
        </>
    );
};

export default LoginModal;