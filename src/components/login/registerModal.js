import React from 'react';
import Button from '../shared/button/button';
import { useAuth } from '../../context/use-auth';
import { Dialog } from "@reach/dialog";
import VisuallyHidden from "@reach/visually-hidden";
import "@reach/dialog/styles.css";
import { CircleButton, FormGroup } from '../shared/lib'
import './modal.scss'

const RegisterModal = (props) => {
    const auth = useAuth();
    const [isOpen, setIsOpen] = React.useState(false)

    const handleRegister = (e) => {
        e.preventDefault();
        const email = document.querySelector('#email').value
        const password = document.querySelector('#password').value
        console.log('registering', email, password)
        auth.register(email, password)
    }

    const open = () => { console.log('opening'); setIsOpen(true) };
    const close = () => setIsOpen(false);

    return (
        <>
            <Button name='Register' onClickHandler={open} className='button'>Register</Button>
            <Dialog isOpen={isOpen} onDismiss={close} aria-label='register'>
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <CircleButton onClick={() => setIsOpen(false)}>
                        <VisuallyHidden>Close</VisuallyHidden>
                        <span aria-hidden>×</span>
                    </CircleButton>
                </div>
                <div>
                    <h1>Register</h1>
                    <form className='formClass' onSubmit={handleRegister}>
                        <FormGroup>
                            <label htmlFor="password">Email</label>
                            <input type='text' name='email' id='email'></input>
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="password">Password</label>
                            <input type='password' name='password' id='password'></input>
                        </FormGroup>
                        <div>
                            <Button name='Register' active='true' onClickHandler={handleRegister} />
                        </div>
                    </form>
                </div>
            </Dialog>
        </>
    );
};

export default RegisterModal;