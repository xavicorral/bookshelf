import React, { Suspense } from 'react';
import { useAuth } from './context/use-auth';
import { FullPageSpinner } from './components/shared/lib';
import './styles/global.scss';

const Logged = React.lazy(() => import('./screens/logged'))
const NotLogged = React.lazy(() => import('./screens/notLogged'))

function App() {
  let auth = useAuth();

  return (
    <Suspense fallback={<FullPageSpinner />}>
      {auth.user ?
        <Logged />
        :
        <NotLogged />
      }
    </Suspense>
  );
}

export default App;
