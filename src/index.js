import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { ProvideTheme } from './context/use-theme';
import { ProvideAuth } from './context/use-auth';

ReactDOM.render(
    <ProvideTheme theme='white'>
        <ProvideAuth>
            <App />
        </ProvideAuth>
    </ProvideTheme>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
