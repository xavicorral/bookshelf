import * as firebase from 'firebase/app';
import "firebase/database";
import "firebase/auth";

/*const firebaseConfig = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID
};*/

const firebaseConfig = {
  apiKey: 'AIzaSyBCzlPAOk11yQCEbmBJltBTx7RJV5ubApo',
  authDomain: 'bookshelf-59a50.firebaseapp.com',
  databaseURL: 'https://bookshelf-59a50.firebaseio.com',
  projectId: 'bookshelf-59a50',
  storageBucket: 'bookshelf-59a50.appspot.com',
  messagingSenderId: '252332852780',
  appId: '1:252332852780:web:20492e6ae86bc60c65f389'
};

let firebaseInstance = null;

export const getDatabase = () => {
  if (firebaseInstance === null) {
    firebaseInstance = firebase.initializeApp(firebaseConfig);
  }
  return firebaseInstance.database();
}

export const getAuth = () => {
  if (firebaseInstance === null) {
    firebaseInstance = firebase.initializeApp(firebaseConfig);
  }
  return firebaseInstance.auth();
}

export const getGoogleProvider = () => {
  return new firebase.auth.GoogleAuthProvider();
}