const apiFetch = (search) => {
    const baseUrl = 'https://api.itbook.store/1.0/search/{search}'
    const url = baseUrl.replace('{search}', search)
    const headers = new Headers();
    
    return fetch(url, { headers: headers });
}

export default apiFetch;