import { BOOKS_SHELF } from "../constants/constants";

export const initialShelf = {
    booksReceived: [],
    booksToReadIds: [],
    booksFinishedIds: []
}

export const dumpItemsToLocalstorage = (books) => {
    console.log('dumping books', books)
    localStorage.setItem(BOOKS_SHELF, JSON.stringify(books))
}

export const getItems = () => {
    const booksShelf = JSON.parse(localStorage.getItem(BOOKS_SHELF))
    return booksShelf ? booksShelf : initialShelf;
}

export const getInitialShelf = () => localStorage.getItem(BOOKS_SHELF) ? JSON.parse(localStorage.getItem(BOOKS_SHELF)) : initialShelf

