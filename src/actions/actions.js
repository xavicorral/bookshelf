import {
    FETCH_BOOKS,
    MARK_BOOK_AS_TO_READ_ACTION,
    MARK_BOOK_AS_FINISHED_ACTION,
    DELETE_BOOK_ACTION
} from '../constants/constants'

export const addBookAction = (booksArray) => {
    return {
        type: FETCH_BOOKS,
        payload: booksArray
    }
}

export const readBookAction = (id) => {
    return {
        type: MARK_BOOK_AS_TO_READ_ACTION,
        payload: id
    }
}

export const finishBookAction = (id) => {
    return {
        type: MARK_BOOK_AS_FINISHED_ACTION,
        payload: id
    }
}

export const deleteBookAction = (id) => {
    return {
        type: DELETE_BOOK_ACTION,
        payload: id
    }
}