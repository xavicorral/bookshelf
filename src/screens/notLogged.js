import React from 'react';
import LoginModal from '../components/login/loginModal';
import RegisterModal from '../components/login/registerModal';
import { useTheme } from '../context/use-theme';
import './notLogged.scss';

const NotLogged = () => {

    const theme = useTheme()

    return (
        <div className="App" style={{ backgroundColor: theme.background }}>
            <div>
                <img src='/img/books.png' alt='books shelf' className='booksImage' />
                <h1>Bookshelf</h1>
                <div>
                    <LoginModal />
                    <RegisterModal />
                </div>
            </div>

        </div>
    );
};

export default NotLogged;