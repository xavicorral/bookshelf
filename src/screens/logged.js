import React from 'react';
import { Route, Switch, BrowserRouter } from "react-router-dom";
import './logged.scss';
import Menu from '../components/menu/menu';
import Wellcome from '../components/menu/wellcome';
import List from './layouts/list';
import Finished from './layouts/finished';
import Discover from './layouts/discover';
import BookDetail from './layouts/bookDetail';
import { useTheme } from '../context/use-theme';
import { ProviderBooksList } from '../context/use-books-list';

const Logged = () => {
    const theme = useTheme()

    return (
        <div className="loggedScreen" style={{ backgroundColor: theme.background }}>
            <Wellcome />
            <main>
                <BrowserRouter>
                    <Menu />
                    <section>
                        <ProviderBooksList>
                            <Switch>
                                <Route exact path="/list" component={List} />
                                <Route path="/finished" component={Finished} />
                                <Route path="/discover" component={Discover} />
                                <Route path="/book/{id}" component={BookDetail} />
                            </Switch>
                        </ProviderBooksList>
                    </section>
                </BrowserRouter>
            </main>
        </div>
    );
};

export default Logged;