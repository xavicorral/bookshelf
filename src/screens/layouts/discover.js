import React, { useEffect, useState } from 'react';
import BooksList from '../../components/booksList/booksList';
import SearchToolbar from '../../components/searchToolbar/searchToolbar';
import apiFetch from '../../services/apiFetchService';
import { FaSpinner } from 'react-icons/fa';
import { useBooksList } from '../../context/use-books-list';
import { addBookAction, readBookAction } from '../../actions/actions';

const Discover = () => {

    let [books, setBooks] = useState(null);
    let [error, setError] = useState();
    let [loading, setLoading] = useState(false);
    let [searchTerm, setSearchTerm] = useState('');
    const booksList = useBooksList()
    const { dispatch } = booksList

    useEffect(() => {

        if (!searchTerm) {
            return
        }

        setLoading(true)
        setBooks(null)

        apiFetch(searchTerm)
            .then(res => res.json())
            .then(result => {
                const booksAction = addBookAction(result.books);
                dispatch(booksAction)
                setBooks(result.books);
                setError(null);
                setLoading(false);
            })
            .catch(error => {
                console.error(error)
                setBooks(null);
                setError(error);
                setLoading(false);
            })
            .finally(console.log('finally reached'))

    }, [searchTerm, dispatch])

    const handleToolbarSubmit = (toolbarSearchTerm) => {
        setSearchTerm(toolbarSearchTerm)
    }

    const handleAddItem = (book) => {
        console.log('adding book ', book)
        dispatch(readBookAction(book))
    }

    return (
        <>
            <SearchToolbar handleToolbarSubmit={handleToolbarSubmit} />
            {loading ? <p className='message'><FaSpinner /></p> : null}
            {error ? <p className='message'>Oupss something went wrong! </p> : null}
            {searchTerm && !loading && !error ?
                (books && books.length > 0)
                    ?
                    <p className='message'>Here you go! Find more books with the search bar above.</p>
                    :
                    <p className='message'>Sorry, we did not find any books matching your criteria.</p>
                :
                null
            }
            <BooksList books={books} handleAddItem={handleAddItem} listType='discover' />
        </>
    );
};

export default Discover;