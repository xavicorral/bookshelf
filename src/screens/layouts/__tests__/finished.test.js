import React from 'react';
import { render } from '@testing-library/react';
import Finished from '../finished';
import * as useBooksContext from '../../../context/use-books-list';

jest.mock('react-router-dom', () => ({
    Link: (props) => (<a href={props.to}>{props.children}</a>)
}));

useBooksContext.useBooksList = jest.fn().mockReturnValue(
    {
        state: { booksReceived: [], booksFinishedIds: [] },
    });

test('renders Finished component', () => {
    const { getByText } = render(<Finished />);
    const emailElement = getByText(/Wellcome, pepito@gma/i);
    expect(emailElement).toBeInTheDocument();
});