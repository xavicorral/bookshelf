import React from 'react';
import BooksList from '../../components/booksList/booksList';
import Teaser from '../../components/teaser/teaser';
import { useBooksList } from '../../context/use-books-list';

const List = () => {
    const { state } = useBooksList()
    const { booksReceived, booksToReadIds } = state
    const booksToRead = booksReceived.filter(book => booksToReadIds.includes(book.isbn13))

    return (
        <>
            {booksToRead.length !== 0 ? null : <Teaser path={'list'} />}
            <BooksList books={booksToRead} listType='list' />
        </>
    );
};

export default List;