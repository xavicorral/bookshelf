import React from 'react';
import BooksList from '../../components/booksList/booksList';
import Teaser from '../../components/teaser/teaser';
import { useBooksList } from '../../context/use-books-list';

const Finished = () => {

    const { state } = useBooksList()
    const { booksReceived, booksFinishedIds } = state
    const booksFinished = booksReceived.filter(book => booksFinishedIds.includes(book.isbn13))

    return (
        <>
            {booksFinished.length !== 0 ? null : <Teaser path={'finished'} />}
            <BooksList books={booksFinished} listType='finished' />
        </>
    );
};

export default Finished;