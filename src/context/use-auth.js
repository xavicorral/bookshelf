import React, { useState, useEffect } from 'react';
import { getAuth } from '../services/firebaseService';

const AuthContext = React.createContext()

function useProvideAuth() {
    const [user, setUser] = useState(null);

    const register = (email, password) => {
        getAuth()
            .createUserWithEmailAndPassword(email, password)
            .catch(function (error) {
                let errorCode = error.code;
                let errorMessage = error.message;
                if (errorCode === 'auth/weak-password') {
                    alert('The password is too weak.');
                } else {
                    alert(errorMessage);
                }
                console.log(error);
            });
    }

    const signIn = (email, password) => {
        getAuth()
            .signInWithEmailAndPassword(email, password)
            .then(() => console.log('Correctly signed in'))
            .catch(error => {
                const errorCode = error.code;
                const errorMessage = error.message;
                console.log(error)
                if (errorCode === 'auth/wrong-password') {
                    alert('Wrong password.');
                } else {
                    alert(errorMessage);
                }
            });
    }

    const signOut = () => {
        getAuth()
            .signOut()
            .then(function () {
                console.log('logged out')
            }).catch(function (error) {
                console.error('User could not be logged out');
                console.error(error);
            });
    }

    useEffect(() => {
        const unsubscribe = getAuth().onAuthStateChanged(user => {
            if (user) {
                setUser(user);
            } else {
                setUser(false);
            }
        });
        return () => unsubscribe();
    }, []);

    return { user, register, signIn, signOut }
}

const ProvideAuth = (props) => {
    const auth = useProvideAuth();
    return <AuthContext.Provider value={auth}>{props.children}</AuthContext.Provider>
}

const useAuth = () => React.useContext(AuthContext)

export { ProvideAuth, useAuth };