import React, { useReducer } from 'react';
import { useContext } from 'react';
import * as localStorageService from '../services/localStorageService';
import { removeDuplicates } from '../components/shared/lib';
import {
    FETCH_BOOKS,
    MARK_BOOK_AS_TO_READ_ACTION,
    MARK_BOOK_AS_FINISHED_ACTION,
    DELETE_BOOK_ACTION
} from '../constants/constants';

const BooksListContext = React.createContext();

const booksReducer = (state, action) => {
    let newState, booksReceivedIds, newFetchedBooks, newBooksToReadIds, newBooksFinishedIds, newBooksIds;
    console.log('dispatching action with state', action, state)
    switch (action.type) {
        case FETCH_BOOKS:
            booksReceivedIds = state.booksReceived.map(book => book.isbn13)
            newFetchedBooks = action.payload.filter(book => !booksReceivedIds.includes(book.isbn13))
            newState = { ...state, booksReceived: state.booksReceived.concat(newFetchedBooks) }
            break
        case MARK_BOOK_AS_TO_READ_ACTION:
            newState = { ...state, booksToReadIds: removeDuplicates(state.booksToReadIds.concat(action.payload)) }
            break
        case MARK_BOOK_AS_FINISHED_ACTION:
            newBooksIds = state.booksToReadIds.filter(bookId => bookId !== action.payload)
            newState = { ...state, booksToReadIds: newBooksIds, booksFinishedIds: removeDuplicates(state.booksFinishedIds.concat(action.payload)) }
            break
        case DELETE_BOOK_ACTION:
            newBooksToReadIds = state.booksToReadIds.filter(bookId => bookId !== action.payload)
            newBooksFinishedIds = state.booksFinishedIds.filter(bookId => bookId !== action.payload)
            newState = { ...state, booksToReadIds: newBooksToReadIds, booksFinishedIds: newBooksFinishedIds }
            break
        default:
            return state
    }

    localStorageService.dumpItemsToLocalstorage(newState)
    return newState;
}

function useProviderBooks() {
    const initialShelf = localStorageService.getInitialShelf()
    const [state, dispatch] = useReducer(booksReducer, initialShelf)
    console.log(state)
    return { state, dispatch }
}

const ProviderBooksList = (props) => {
    return <BooksListContext.Provider value={useProviderBooks()}>{props.children}</BooksListContext.Provider>
}

const useBooksList = () => useContext(BooksListContext)

export { ProviderBooksList, useBooksList }