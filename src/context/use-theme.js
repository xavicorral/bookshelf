import React from 'react';

const themes = {
    white: {
        foreground: "#000000",
        background: "#ffffff"
    },
    light: {
        foreground: "#000000",
        background: "#eff0f5"
    },
    dark: {
        foreground: "#ffffff",
        background: "#222222"
    }
};

const ThemeContext = React.createContext()

const ProvideTheme = (props) => {
    return <ThemeContext.Provider value={themes[props.theme]}>{props.children}</ThemeContext.Provider>
}

const useTheme = () => React.useContext(ThemeContext)

export { ProvideTheme, useTheme };